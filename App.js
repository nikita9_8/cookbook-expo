import 'react-native-gesture-handler';
import 'react-native-get-random-values';
import React, { useState } from 'react';
import { AppLoading } from 'expo';
import { Provider } from 'react-redux';
import store from './src/store';

import { NavigationDrawer } from './src/navigation/AppNavigation';
import { bootstrap } from './src/bootstrap';

export default function App() {
    const [isReady, setIsReady] = useState(false);

    if (!isReady) {
        return (
            <AppLoading
                startAsync={bootstrap}
                onFinish={() => setIsReady(true)}
                onError={(err) => console.log(err)}
            />
        );
    } else {
	    return (
		    <Provider store={store}>
			    <NavigationDrawer />
		    </Provider>
	    );
    }
}
