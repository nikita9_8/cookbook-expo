import * as Font from 'expo-font';
import { DB } from './db';
import { Ionicons } from '@expo/vector-icons';

export async function bootstrap() {
		await Font.loadAsync({
			Roboto: require('native-base/Fonts/Roboto.ttf'),
			Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
			loraBold: require('../assets/fonts/Lora-Bold.ttf'),
			loraRegular: require('../assets/fonts/Lora-Regular.ttf'),
			...Ionicons.font,
		});
		try {
			await DB.init();
			console.log('DATABASE started')
		} catch (e) {
			console.log('Error: custom --- ', e);
		}
}
