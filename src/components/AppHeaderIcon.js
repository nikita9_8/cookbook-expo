import React from 'react';
import { HeaderButton } from 'react-navigation-header-buttons';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import { THEME } from '../theme';

export const AppHeaderIcon = (props) => (
    <HeaderButton
	    {...props}
	    IconComponent={Ionicons}
	    iconSize={24}
	    color={Platform.OS === 'android' ? THEME.WHITE : THEME.MAIN_COLOR}
    />
);
