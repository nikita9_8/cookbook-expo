import React, { useCallback, useState } from 'react';
import {
    View,
    TextInput,
    StyleSheet,
    FlatList,
    Text,
    Button,
    TouchableOpacity,
    Platform,
    KeyboardAvoidingView,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {
    Container,
    Header,
    Content,
    Input,
    Label,
    Card,
    CardItem,
    Body,
    Item as BaseItem,
} from 'native-base';
import { THEME } from '../theme';

export const IngredientsList = ({
    data,
    onAdd,
    addDescription,
    removeIngredient,
    changeTitle,
}) => {
    const [ingredient, setName] = useState('');

    const addIngredient = () => {
        onAdd(ingredient.trim());
        setName('');
    };

    return (
        <View style={styles.list}>
            <View>
	            <BaseItem style={styles.item} floatingLabel>
		            <Label>Название ингредиента</Label>
		            <Input
			            style={styles.input}
			            value={ingredient}
			            onChangeText={setName}
		            />
	            </BaseItem>
                <Button
                    title="Добавить ингредиент"
                    color={THEME.MAIN_COLOR}
                    onPress={addIngredient}
                    disabled={
                        !ingredient.trim().length ||
                        ingredient.trim().length > 17
                    }
                />
            </View>
            <View>
                <FlatList
                    data={data}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => (
                        <Card>
                            <CardItem>
                                <Body style={styles.ingItem}>
                                    <TextInput
                                        style={{ width: '30%' }}
                                        value={item.name ?? item.ingredient}
                                        onChangeText={(value) => {
                                            changeTitle(value, item.id);
                                        }}
                                    />
                                    <TextInput
                                        style={{ width: '50%' }}
                                        placeholder="Описание ингредиента"
                                        value={item.description}
                                        onChangeText={(value) => {
                                            addDescription(value, item.id);
                                        }}
                                    />
                                    <TouchableOpacity
                                        style={{ width: '8%' }}
                                        onPress={() => {
                                            removeIngredient(item.id);
                                        }}
                                    >
                                        <Ionicons
                                            name="ios-close-circle-outline"
                                            size={22}
                                            color={THEME.DANGER_COLOR}
                                        />
                                    </TouchableOpacity>
                                </Body>
                            </CardItem>
                        </Card>
                    )}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        padding: 5,
        marginBottom: 5,
    },
    list: {
        marginBottom: 10,
    },
    ingItem: {
        alignItems: 'center',
        height: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 2,
    },
});
