import React, { useState } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Button,
    Alert,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import { Text } from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import { THEME } from '../theme';
import { Ionicons } from '@expo/vector-icons';

async function askForPermissions() {
    const { status } = await Permissions.askAsync(
        Permissions.CAMERA,
        Permissions.CAMERA_ROLL
    );

    if (status !== 'granted') {
        Alert.alert(
            'Предупреждение',
            'Для создания фото нужно дать разрешение'
        );
        return false;
    }
    return true;
}

export const PhotoPicker = ({ onPick, img = null, imgSet }) => {
    const takePhoto = async () => {
        const hasPermissions = await askForPermissions();

        if (!hasPermissions) {
            return;
        }

        const { uri } = await ImagePicker.launchCameraAsync({
            quality: 1,
            allowsEditing: true,
            aspect: [16, 9],
        });

        imgSet(uri);
        onPick(uri);
    };

    const openImagePickerAsync = async () => {
        let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();

        if (permissionResult.granted === false) {
            alert('Для использования галереи нужно разрешить доступ');
            return;
        }

        let pickerResult = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [16, 9],
            quality: 1,
        });

        if (pickerResult.cancelled === true) {
            return;
        }

        imgSet(pickerResult.uri);
        onPick(pickerResult.uri);
    };

    const removeImg = () => {
        imgSet(null);
    };

    return (
        <View style={styles.wrapper}>
            {img?.length > 0 && (
                <ImageBackground style={styles.image} source={{ uri: img }}>
                    <TouchableOpacity
                        style={{ width: '10%' }}
                        onPress={() => {
                            removeImg();
                        }}
                    >
                        <Ionicons
                            name="ios-close-circle-outline"
                            size={30}
                            color={THEME.DANGER_COLOR}
                        />
                    </TouchableOpacity>
                </ImageBackground>
            )}
            <View style={styles.btnGroup}>
                <TouchableOpacity onPress={takePhoto} style={styles.button}>
                    <Text style={styles.buttonText}>Сделать фото</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={openImagePickerAsync}
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>Галерея</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        marginBottom: 10,
    },
    image: {
        alignItems: 'flex-end',
        paddingTop: 5,
        width: '100%',
        height: 200,
        marginTop: 10,
    },
    btnGroup: {
        flex: 1,
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    button: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: THEME.MAIN_COLOR,
        marginHorizontal: 5,
        height: 35,
        borderRadius: 5,
    },
    buttonText: {
        color: THEME.WHITE,
        textAlign: 'center',
        width: '100%',
    },
});
