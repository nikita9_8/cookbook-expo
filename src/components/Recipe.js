import React from 'react';
import {
    View,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
} from 'react-native';
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Thumbnail,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Right,
} from 'native-base';
import { Ionicons } from '@expo/vector-icons';

export const Recipe = ({ recipe, onOpen }) => {
    const { imgMain, date, title, cookTime } = recipe;

    return (
        <TouchableOpacity activeOpacity={0.7} onPress={() => onOpen(recipe)}>
            <Card>
                <CardItem>
                    <Body>
                        <Text>{title}</Text>
                    </Body>
                </CardItem>
                <CardItem cardBody>
                    <Image
                        source={
                            imgMain
                                ? { uri: imgMain }
                                : require('../../assets/img/def_recipe.jpg')
                        }
                        style={{ height: 200, width: '100%', flex: 1 }}
                    />
                </CardItem>
                <CardItem>
                    <Left style={styles.recipeDate}>
                        <Ionicons name="md-calendar" size={25} />
                        <Text>{new Date(date).toLocaleDateString()}</Text>
                    </Left>
	                {cookTime.length > 0 && (<Right style={styles.recipeTime}>
		                <Ionicons name="md-time" size={25} />
		                <Text style={{ marginLeft: 5 }}>{cookTime}</Text>
	                </Right>)}
                </CardItem>
            </Card>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    recipeDate: { flexDirection: 'row', alignItems: 'center' },
    recipeTime: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
});
