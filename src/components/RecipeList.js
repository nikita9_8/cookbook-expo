import React from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native';

import { Recipe } from './Recipe';

export const RecipeList = ({ data, onOpen }) => {
    if (!data.length) {
        return (
            <View style={styles.wrapper}>
                <Text style={styles.noItems}>Список рецептов пуст</Text>
            </View>
        );
    }

    return (
        <View style={styles.wrapper}>
            <FlatList
                data={data}
                keyExtractor={(recipe) => recipe.id.toString()}
                renderItem={({ item }) => (
                    <Recipe recipe={item} onOpen={onOpen} />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        padding: 10,
    },
    noItems: {
        fontFamily: 'loraRegular',
        textAlign: 'center',
        marginVertical: 10,
        fontSize: 18,
    },
});
