export const DATA = [
    {
        id: '1',
        imgMain:
            'https://media.istockphoto.com/photos/healhty-vegan-lunch-bowl-avocado-quinoa-sweet-potato-tomato-spinach-picture-id893716434',
        title: 'Блинчики',
        description:
            'Lorem Ipsum - це текст-"риба", що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною "рибою" аж з XVI сторіччя, коли невідомий друкар взяв шрифтову гранку та склав на ній підбірку зразків шрифтів. "Риба" не тільки успішно пережила п\'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп\'ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.',
        date: new Date().toJSON(),
        ingredients: {
            banana: '2 целых',
            oil: '2 столовых ложки',
            cabbage: '1 целый',
            carrot: '2 очищенных и  помытых',
            potato: '1 кг мытой',
            flour: '1 стакан (330г)',
        },
        cookTime: '25m',
        kcal: 350,
        tags: ['fast', 'pretty', 'tasty'],
        booked: true,
    },
    {
        id: '2',
        imgMain:
            'https://media.istockphoto.com/photos/roasted-pumpkin-salad-with-spinach-and-walnut-top-view-picture-id613666608',
        title: 'Мясо по французки',
        description:
            'Lorem Ipsum - це текст-"риба", що використо. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп\'ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.',
        date: new Date().toJSON(),
        ingredients: {
            banana: '2 целых',
            oil: '2 столовых ложки',
            cabbage: '1 целый',
        },
        cookTime: '1h',
        kcal: 500,
        tags: ['fast', 'tasty'],
        booked: true,
    },
    {
        id: '3',
        imgMain:
            'https://media.istockphoto.com/photos/stuffed-butternut-squash-with-chickpeas-cranberries-quinoa-cooked-in-picture-id1034584212',
        title: 'Жульен',
        description:
            'Lorem Ipsum. "Риба" не тільки успішно пережила п\'ять століть, але й прижилася в електронному верстуванні, залишаючись по суті незмінною. Вона популяризувалась в 60-их роках минулого сторіччя завдяки виданню зразків шрифтів Letraset, які містили уривки з Lorem Ipsum, і вдруге - нещодавно завдяки програмам комп\'ютерного верстування на кшталт Aldus Pagemaker, які використовували різні версії Lorem Ipsum.',
        date: new Date().toJSON(),
        ingredients: {
            carrot: '2 очищенных и  помытых',
            potato: '1 кг мытой',
            flour: '1 стакан (330г)',
        },
        cookTime: '50m',
        kcal: 150,
        tags: ['fast'],
        booked: false,
    },
];
