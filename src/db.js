import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('test2.db');

export class DB {
    static init() {
        return new Promise((res, rej) => {
            db.exec(
                [{ sql: 'PRAGMA foreign_keys = ON;', args: [] }],
                false,
                () => {
                    db.transaction((tx) => {
                        tx.executeSql(
                            `
			                    create table if not exists recipes
								(
								    id          integer not null
								        constraint recipes_pk
								            primary key autoincrement,
								    imgMain     text default '',
								    title       text    not null,
								    description text    not null,
								    date        text    not null,
								    cookTime    text,
								    kcal        integer,
								    booked      integer not null
								)`,
                            [],
                            () => {
                                tx.executeSql(
                                    `create table if not exists ingredients
									(
									    id          integer not null
									        constraint ingredients_pk
									            primary key autoincrement,
									    recipe_id integer not null
									        references recipes
									            on delete cascade,
									    ingredient  text not null,
									    description text
									)`,
                                    [],
                                    () => {
                                        tx.executeSql(
                                            `create table if not exists tags
											(
											    id        integer not null
											        constraint tags_pk
											            primary key autoincrement,
											    tag       text    not null,
											    recipe_id integer     not null
											        references recipes
									            on delete cascade
											)`,
                                            [],
                                            (_, result) => res(result),
                                            (_, error) => rej(error)
                                        );
                                    },
                                    (_, error) => rej(error)
                                );
                            },
                            (_, error) => rej(error)
                        );
                    });
                }
            );
        });
    }

    static getRecipes() {
        return new Promise((res, rej) => {
            const allData = [];
            db.transaction((tx) => {
                tx.executeSql(
                    'select * from recipes',
                    [],
                    (_, result) => {
                        result.rows._array.forEach((item) => {
                            allData.push(
                                new Promise((resolve, reject) => {
                                    tx.executeSql(
                                        `select * from ingredients where recipe_id = ?`,
                                        [item.id],
                                        (_ingredients, ingredientsResult) => {
                                            resolve({
                                                ...item,
                                                ingredients:
                                                    ingredientsResult.rows
                                                        ._array,
                                            });
                                        },
                                        (_, error) => reject(error)
                                    );
                                })
                            );
                        });
                        res(Promise.all(allData));
                    },
                    (_, error) => rej(error)
                );
            });
        });
    }

    static getRecipe(id) {
        return new Promise((res, rej) => {
            const allData = [];
            db.transaction((tx) => {
                tx.executeSql(
                    'select * from recipes where id = ?',
                    [id],
                    (_, result) => {
                        result.rows._array.forEach((item) => {
                            allData.push(
                                new Promise((resolve, reject) => {
                                    tx.executeSql(
                                        `select * from ingredients where recipe_id = ?`,
                                        [id],
                                        (_ingredients, ingredientsResult) => {
                                            resolve({
                                                ...item,
                                                ingredients:
                                                    ingredientsResult.rows
                                                        ._array,
                                            });
                                        },
                                        (_, error) => reject(error)
                                    );
                                })
                            );
                        });
                        res(Promise.all(allData));
                    },
                    (_, error) => rej(error)
                );
            });
        });
    }

    static createPost(recipe) {
        const {
            title,
            description,
            imgMain,
            date,
            ingredients,
            cookTime,
            kcal,
        } = recipe;
        return new Promise((res, rej) => {
            db.transaction((tx) => {
                tx.executeSql(
                    `insert into recipes (title, description, date, booked, imgMain, cookTime, kcal) values (?, ?, ?, ?, ?, ?, ?)`,
                    [title, description, date, 0, imgMain, cookTime, kcal],
                    (_, result) => res(result.insertId),
                    (_, err) => rej(err)
                );
            });
        });
    }

    static createIngredients({ ingredients, id }) {
        const insertes = [];
        ingredients.forEach((ingredItem) => {
            const { name = ingredItem.ingredient, description } = ingredItem;
            insertes.push(
                new Promise((res, rej) => {
                    db.transaction((tx) => {
                        tx.executeSql(
                            `insert into ingredients (recipe_id, ingredient, description) values (?, ?, ?)`,
                            [id, name, description],
                            (_, result) => {
                                tx.executeSql(
                                    `select * from ingredients where id = ?`,
                                    [result.insertId],
                                    (_, resultSelect) => {
                                        res(resultSelect.rows._array[0]);
                                    },
                                    (_, err) => rej(err)
                                );
                            },
                            (_, err) => rej(err)
                        );
                    });
                })
            );
        });

        return Promise.all(insertes);
    }

    static updateRecipe(recipe) {
        return new Promise((res, rej) => {
            db.transaction((tx) => {
                tx.executeSql(
                    `update recipes set booked = ? where id = ?`,
                    [recipe.booked ? 0 : 1, recipe.id],
                    res,
                    (_, error) => rej(error)
                );
            });
        });
    }

    static updateFullRecipe({
        cookTime,
        date,
        description,
        id,
        imgMain,
        ingredients = [],
        kcal,
        title,
    }) {
        return new Promise((res, rej) => {
            db.transaction((tx) => {
                tx.executeSql(
                    `update recipes set imgMain = ?, title = ?, description = ?, date = ?, cookTime = ?, kcal = ? where id = ? `,
                    [
                        imgMain,
                        title,
                        description,
                        date,
                        cookTime,
                        kcal,
                        id,
                    ],
                    (_, result) => {
                        tx.executeSql(
                            `delete from ingredients where recipe_id = ?`,
                            [id],
                            (_, result) => {
                                res(
                                    DB.createIngredients({
                                        ingredients,
                                        id,
                                    }).then(() => DB.getRecipe(id))
                                );
                            },
                            (_, error) => rej(error)
                        );
                    },
                    (_, error) => rej(error)
                );
            });
        });
    }

    static removeRecipe(id) {
        return new Promise((res, rej) => {
	        db.exec(
		        [{ sql: 'PRAGMA foreign_keys = ON;', args: [] }],
		        false,
		        () => {
			        db.transaction((tx) => {
				        tx.executeSql(
					        `delete from recipes where id = ?`,
					        [id],
					        res,
					        (_, error) => rej(error)
				        );
			        })
		        });
        });
    }
}
