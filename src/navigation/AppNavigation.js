import React from 'react';
import { Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { MainScreen } from '../screens/MainScreen';
import { RecipeScreen } from '../screens/RecipeScreen';
import { THEME } from '../theme';
import { BookmarkedScreen } from '../screens/BookmarkedScreen';
import { AboutScreen } from '../screens/AboutScreen';
import { CreateScreen } from '../screens/CreateScreen';
import { SearchScreen } from '../screens/SearchScreen';
import { EditScreen } from '../screens/EditScreen';

const RecipeStack = createStackNavigator();

const AppNavigation = () => (
    <RecipeStack.Navigator
        initialRouteName="Home"
        screenOptions={{
            headerStyle: {
                ...Platform.select({
                    ios: {
                        backgroundColor: THEME.WHITE,
                    },
                    android: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                    default: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                }),
            },
            headerTintColor: THEME.WHITE,
        }}
    >
        <RecipeStack.Screen
            name="Home"
            options={{ title: 'Рецепты' }}
            component={MainScreen}
        />
        <RecipeStack.Screen
            name="Recipe"
            component={RecipeScreen}
            options={{ title: 'Рецепт номер 1' }}
        />
        <RecipeStack.Screen
            name="Search"
            component={SearchScreen}
            options={{ title: 'Поиск' }}
        />
        <RecipeStack.Screen
            name="Edit"
            component={EditScreen}
            options={{ title: 'Редактировать' }}
        />
    </RecipeStack.Navigator>
);

const Booked = createStackNavigator();

const BookedNavigation = () => (
    <Booked.Navigator
        initialRouteName="Booked"
        screenOptions={{
            headerStyle: {
                ...Platform.select({
                    ios: {
                        backgroundColor: THEME.WHITE,
                    },
                    android: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                    default: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                }),
            },
            headerTintColor: THEME.WHITE,
        }}
    >
        <Booked.Screen
            name="Booked"
            options={{ title: 'Рецепты' }}
            component={BookmarkedScreen}
        />
        <Booked.Screen
            name="Recipe"
            component={RecipeScreen}
            options={{ title: 'Рецепт номер 1' }}
        />
        <RecipeStack.Screen
            name="Search"
            component={SearchScreen}
            options={{ title: 'Поиск' }}
        />
        <RecipeStack.Screen
            name="Edit"
            component={EditScreen}
            options={{ title: 'Редактировать' }}
        />
    </Booked.Navigator>
);

const Tab =
    Platform.OS === 'android'
        ? createMaterialBottomTabNavigator()
        : createBottomTabNavigator();

const NavigationTabs = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Recipes') {
                    iconName = focused ? 'ios-albums' : 'ios-albums';
                } else if (route.name === 'Booked') {
                    iconName = focused ? 'ios-star' : 'ios-star';
                }

                return <Ionicons name={iconName} size={20} color={color} />;
            },
        })}
        {...(Platform.OS === 'android'
            ? {
                  activeTintColor: THEME.MAIN_COLOR,
                  shifting: true,
                  barStyle: { backgroundColor: THEME.MAIN_COLOR },
              }
            : {
                  tabBarOptions: {
                      activeTintColor: THEME.MAIN_COLOR,
                      inactiveTintColor: THEME.GRAY,
                  },
              })}
    >
        <Tab.Screen
            name="Recipes"
            component={AppNavigation}
            options={{ title: 'Все' }}
        />
        <Tab.Screen
            name="Booked"
            component={BookedNavigation}
            options={{ title: 'Избранные' }}
        />
    </Tab.Navigator>
);

const About = createStackNavigator();

const AboutNavigation = () => (
    <About.Navigator
        screenOptions={{
            headerStyle: {
                ...Platform.select({
                    ios: {
                        backgroundColor: THEME.WHITE,
                    },
                    android: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                    default: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                }),
            },
            headerTintColor: THEME.WHITE,
        }}
    >
        <About.Screen
            name="About"
            options={{ title: 'О приложении' }}
            component={AboutScreen}
        />
    </About.Navigator>
);

const Create = createStackNavigator();

const CreateNavigation = () => (
    <Create.Navigator
        screenOptions={{
            headerStyle: {
                ...Platform.select({
                    ios: {
                        backgroundColor: THEME.WHITE,
                    },
                    android: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                    default: {
                        backgroundColor: THEME.MAIN_COLOR,
                    },
                }),
            },
            headerTintColor: THEME.WHITE,
        }}
    >
        <Create.Screen
            name="CreateScreen"
            options={{ title: 'Создать рецепт' }}
            component={CreateScreen}
        />
    </Create.Navigator>
);

const Drawer = createDrawerNavigator();

export const NavigationDrawer = () => (
    <NavigationContainer>
        <Drawer.Navigator
	        drawerContentOptions={{
		        activeTintColor: THEME.MAIN_COLOR,
	        }}
        >
            <Drawer.Screen
                name="Main"
                component={NavigationTabs}
                options={{
                    title: 'Главная',
                }}
            />
            <Drawer.Screen
                name="About"
                component={AboutNavigation}
                options={{
                    title: 'О приложение',
                }}
            />
            <Drawer.Screen
                name="Create"
                component={CreateNavigation}
                options={{
                    title: 'Создать рецепт',
                }}
            />
        </Drawer.Navigator>
    </NavigationContainer>
);
