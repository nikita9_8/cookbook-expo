import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { AppHeaderIcon } from '../components/AppHeaderIcon';

export const AboutScreen = ({ navigation }) => {
    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => (
                <HeaderButtons
                    HeaderButtonComponent={AppHeaderIcon}
                    title="Main icon left"
                >
                    <Item
                        title="Take recipe"
                        iconName="ios-menu"
                        onPress={() => navigation.toggleDrawer()}
                    />
                </HeaderButtons>
            ),
        });
    }, []);

    return (
        <View style={styles.center}>
            <Text style={styles.centerText}>
                Данное приложение создано для хранения ваших любимых рецептов,
                чтобы вы могли наслаждаться вкусной едой в любое время
            </Text>
            <Text>
                Версия приложения <Text style={styles.version}>1.0.0</Text>
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    version: {
        fontFamily: 'loraBold',
	    fontWeight: 'bold',
    },
    centerText: {
        textAlign: 'center',
    },
});
