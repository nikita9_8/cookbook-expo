import React from 'react';
import { useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import { AppHeaderIcon } from '../components/AppHeaderIcon';
import { RecipeList } from '../components/RecipeList';

export const BookmarkedScreen = ({ navigation }) => {
    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Избранные рецепты',
            headerLeft: () => (
                <HeaderButtons
                    HeaderButtonComponent={AppHeaderIcon}
                    title="Booked main icon left"
                >
                    <Item
                        title="Take recipe"
                        iconName="ios-menu"
                        onPress={() => navigation.toggleDrawer()}
                    />
                </HeaderButtons>
            ),
        });
    }, []);

    const openRecipeHandler = (recipe) => {
        navigation.navigate('Recipe', {
            recipeId: recipe.id,
            recipeTitle: recipe.title,
        });
    };

    const bookedRecipes = useSelector(state => state.recipe.bookedRecipes);

    return <RecipeList data={bookedRecipes} onOpen={openRecipeHandler} />;
};
