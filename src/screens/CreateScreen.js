import React, { useState, useRef, useCallback } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Button,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    Dimensions,
    KeyboardAvoidingView,
    Platform,
} from 'react-native';
import {
    Container,
    Header,
    Content,
    Input,
    Label,
    Item as BaseItem,
    Textarea,
} from 'native-base';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useDispatch } from 'react-redux';

import { AppHeaderIcon } from '../components/AppHeaderIcon';
import { THEME } from '../theme';
import { addRecipe } from '../store/actions/recipeAction';
import { PhotoPicker } from '../components/PhotoPicker';
import { IngredientsList } from '../components/IngredientsList';
import { KeyboardShift } from '../components/KeyboardShift';

export const CreateScreen = ({ navigation }) => {
    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => (
                <HeaderButtons
                    HeaderButtonComponent={AppHeaderIcon}
                    title="Main icon left"
                >
                    <Item
                        title="Take recipe"
                        iconName="ios-menu"
                        onPress={() => navigation.toggleDrawer()}
                    />
                </HeaderButtons>
            ),
        });
    }, []);

    const [newRecipe, setRecipe] = useState({
        title: '',
        description: '',
        ingredients: [],
        cookTime: '',
        kcal: '',
        tags: [],
        booked: false,
    });
    const [img, imgSet] = useState(null);
    const dispatch = useDispatch();

    const saveHandler = () => {
        const recipe = {
            ...newRecipe,
            imgMain: img,
            date: new Date().toJSON(),
        };
        imgSet(null);
        dispatch(addRecipe(recipe));
        navigation.navigate('Home');
        setRecipe({
            title: '',
            description: '',
            ingredients: [],
            cookTime: '',
            kcal: '',
            tags: [],
            booked: false,
        });
    };

    const photoPickHandler = (uri) => {
        imgSet(uri);
    };

    const setTitle = (value) => {
        setRecipe({ ...newRecipe, title: value });
    };

    const setDescription = (value) => {
        setRecipe({ ...newRecipe, description: value });
    };

    const setCookTime = (value) => {
        setRecipe({ ...newRecipe, cookTime: value });
    };

    const setKcal = (value) => {
        setRecipe({ ...newRecipe, kcal: value });
    };

    const addIngredient = (value) => {
        setRecipe({
            ...newRecipe,
            ingredients: [
                {
                    id: new Date().getTime(),
                    name: value,
                    description: '',
                },
                ...newRecipe.ingredients,
            ],
        });
    };

    const addIngredientDescription = (value, id) => {
        const ingredientIndex = newRecipe.ingredients.findIndex(
            (item) => item.id === id
        );

        if (ingredientIndex !== -1) {
            newRecipe.ingredients[ingredientIndex].description = value;
            setRecipe({ ...newRecipe });
        }
    };

    const changeIngredientName = (value, id) => {
        const ingredientIndex = newRecipe.ingredients.findIndex(
            (item) => item.id === id
        );

        if (ingredientIndex !== -1) {
            newRecipe.ingredients[ingredientIndex].name = value;
            setRecipe({ ...newRecipe });
        }
    };

    const removeIngredientFromRecipe = (id) => {
        setRecipe({
            ...newRecipe,
            ingredients: newRecipe.ingredients.filter((item) => item.id !== id),
        });
    };

    return (
        <KeyboardShift>
            {() => (
                <ScrollView style={{ backgroundColor: '#ffffff' }}>
                    <View>
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <View style={styles.wrapper}>
                                <Text style={styles.title}>Новый рецепт</Text>
                                <PhotoPicker
                                    onPick={photoPickHandler}
                                    img={img}
                                    imgSet={imgSet}
                                />
                                <View style={styles.info}>
                                    <BaseItem style={styles.item} floatingLabel>
                                        <Label>Название рецепта</Label>
                                        <Input
                                            value={newRecipe.title}
                                            onChangeText={setTitle}
                                        />
                                    </BaseItem>
                                    <BaseItem style={styles.item} floatingLabel>
                                        <Label>Время приготовления блюда</Label>
                                        <Input
                                            value={newRecipe.cookTime}
                                            onChangeText={setCookTime}
                                        />
                                    </BaseItem>
                                    <BaseItem style={styles.item} floatingLabel>
                                        <Label>
                                            Каллорийность блюда (на 100г)
                                        </Label>
                                        <Input
                                            value={newRecipe.kcal}
                                            onChangeText={setKcal}
                                        />
                                    </BaseItem>
                                    <Textarea
                                        style={styles.item}
                                        rowSpan={6}
                                        value={newRecipe.description}
                                        onChangeText={setDescription}
                                        bordered
                                        placeholder="Опишите рецепт"
                                    />
                                </View>
                                <IngredientsList
                                    onAdd={addIngredient}
                                    addDescription={addIngredientDescription}
                                    changeTitle={changeIngredientName}
                                    removeIngredient={
                                        removeIngredientFromRecipe
                                    }
                                    data={newRecipe.ingredients}
                                />
                                <Button
                                    title="Сохранить рецепт"
                                    color={THEME.MAIN_COLOR}
                                    onPress={saveHandler}
                                    disabled={
                                        !newRecipe.title ||
                                        !newRecipe.description
                                    }
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </ScrollView>
            )}
        </KeyboardShift>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        padding: 10,
    },
    info: {
        marginVertical: 25,
    },
    item: {
        marginVertical: 5,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'loraRegular',
        marginVertical: 10,
    },
    textarea: {
        padding: 10,
        marginBottom: 10,
    },
});
