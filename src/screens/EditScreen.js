import React, { useState, useRef } from 'react';
import {
    View,
    ScrollView,
    TouchableWithoutFeedback,
    Keyboard,
    TextInput,
    Button,
    StyleSheet,
    ActivityIndicator,
} from 'react-native';
import {
    Container,
    Header,
    Content,
    Input,
    Label,
    Item as BaseItem,
    Textarea,
    Text,
} from 'native-base';

import { useDispatch, useSelector } from 'react-redux';

import { addRecipe, editRecipe } from '../store/actions/recipeAction';
import { PhotoPicker } from '../components/PhotoPicker';
import { IngredientsList } from '../components/IngredientsList';
import { THEME } from '../theme';
import { KeyboardShift } from '../components/KeyboardShift';

export const EditScreen = ({ navigation, route }) => {
    const recipeId = route.params?.recipeId;

    const [newRecipe, setRecipe] = useState(
        useSelector((state) =>
            state.recipe.allRecipes.find((r) => r.id === recipeId)
        )
    );

    const [img, imgSet] = useState(newRecipe.imgMain);
    const dispatch = useDispatch();

    const saveHandler = () => {
        const recipe = {
            ...newRecipe,
            imgMain: img,
            date: new Date().toJSON(),
        };
        imgSet(null);
        dispatch(editRecipe(recipe));
        navigation.navigate('Recipe', {
            recipeId: recipe.id,
        });
    };

    const photoPickHandler = (uri) => {
        imgSet(uri);
    };

    const setTitle = (value) => {
        setRecipe({ ...newRecipe, title: value });
    };

    const setDescription = (value) => {
        setRecipe({ ...newRecipe, description: value });
    };

    const setCookTime = (value) => {
        setRecipe({ ...newRecipe, cookTime: value });
    };

    const setKcal = (value) => {
        setRecipe({ ...newRecipe, kcal: value });
    };

    const addIngredient = (value) => {
        setRecipe({
            ...newRecipe,
            ingredients: [
                {
                    id: new Date().getTime(),
                    name: value,
                    description: '',
                },
                ...newRecipe.ingredients,
            ],
        });
    };

    const addIngredientDescription = (value, id) => {
        const ingredientIndex = newRecipe.ingredients.findIndex(
            (item) => item.id === id
        );

        if (ingredientIndex !== -1) {
            newRecipe.ingredients[ingredientIndex].description = value;
            setRecipe({ ...newRecipe });
        }
    };

    const changeIngredientName = (value, id) => {
        const ingredientIndex = newRecipe.ingredients.findIndex(
            (item) => item.id === id
        );

        if (ingredientIndex !== -1) {
            newRecipe.ingredients[ingredientIndex].name = value;
            setRecipe({ ...newRecipe });
        }
    };

    const removeIngredientFromRecipe = (id) => {
        setRecipe({
            ...newRecipe,
            ingredients: newRecipe.ingredients.filter((item) => item.id !== id),
        });
    };

    if (!newRecipe) {
        return (
            <View style={styles.center}>
                <ActivityIndicator color={THEME.MAIN_COLOR} size="large" />
            </View>
        );
    }

    return (
        <KeyboardShift>
            {() => (
                <ScrollView style={{ backgroundColor: '#ffffff' }}>
                    <View style={{ flex: 1 }}>
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <View style={styles.wrapper}>
                                <Text style={styles.title}>
                                    Изменить рецепт
                                </Text>
                                <PhotoPicker
                                    onPick={photoPickHandler}
                                    img={img}
                                    imgSet={imgSet}
                                />
                                <View style={styles.info}>
                                    <BaseItem style={styles.item} floatingLabel>
                                        <Label>Название рецепта</Label>
                                        <Input
                                            value={
                                                newRecipe?.title.toString() ??
                                                ''
                                            }
                                            onChangeText={setTitle}
                                        />
                                    </BaseItem>
                                    <BaseItem style={styles.item} floatingLabel>
                                        <Label>Время приготовления блюда</Label>
                                        <Input
                                            value={
                                                newRecipe?.cookTime.toString() ??
                                                ''
                                            }
                                            onChangeText={setCookTime}
                                        />
                                    </BaseItem>
                                    <BaseItem style={styles.item} floatingLabel>
                                        <Label>
                                            Каллорийность блюда (на 100г)
                                        </Label>
                                        <Input
                                            value={
                                                newRecipe?.kcal.toString() ?? ''
                                            }
                                            onChangeText={setKcal}
                                        />
                                    </BaseItem>
                                    <Textarea
                                        style={styles.item}
                                        rowSpan={6}
                                        value={
                                            newRecipe?.description.toString() ??
                                            ''
                                        }
                                        onChangeText={setDescription}
                                        bordered
                                        placeholder="Опишите рецепт"
                                    />
                                </View>
                                <IngredientsList
                                    onAdd={addIngredient}
                                    addDescription={addIngredientDescription}
                                    changeTitle={changeIngredientName}
                                    removeIngredient={
                                        removeIngredientFromRecipe
                                    }
                                    data={newRecipe.ingredients}
                                />
                                <Button
                                    title="Сохранить рецепт"
                                    color={THEME.MAIN_COLOR}
                                    onPress={saveHandler}
                                    disabled={
                                        !newRecipe?.title ||
                                        !newRecipe?.description
                                    }
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </ScrollView>
            )}
        </KeyboardShift>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        padding: 10,
    },
    info: {
        marginVertical: 25,
    },
    item: {
        marginVertical: 5,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'loraRegular',
        marginVertical: 10,
    },
    textarea: {
        padding: 10,
        marginBottom: 10,
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
