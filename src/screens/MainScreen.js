import React, { useEffect } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import { AppHeaderIcon } from '../components/AppHeaderIcon';
import { RecipeList } from '../components/RecipeList';
import { loadRecipes, toggleDateFilter } from '../store/actions/recipeAction';
import { THEME } from '../theme';

export const MainScreen = ({ navigation }) => {
    const dispatch = useDispatch();
    const allRecipes = useSelector((state) => state.recipe.allRecipes);
    const loading = useSelector((state) => state.recipe.loading);
    const searchedRules = useSelector((state) => state.recipe.searchRules);
    const filteredRules = useSelector((state) => state.recipe.searchedRecipes);
    const searching = useSelector((state) => state.recipe.searching);

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: 'Рецепты',
            headerRight: () => (
                <HeaderButtons
                    HeaderButtonComponent={AppHeaderIcon}
                    title="Main icon right"
                >
                    <Item
                        title="Search"
                        iconName="ios-search"
                        onPress={() => navigation.navigate('Search')}
                    />
                    <Item
                        title="Date filter"
                        iconName="ios-repeat"
                        onPress={() => dispatch(toggleDateFilter())}
                    />
                    <Item
                        title="Take recipe"
                        iconName="md-create"
                        onPress={() => navigation.navigate('Create')}
                    />
                </HeaderButtons>
            ),
            headerLeft: () => (
                <HeaderButtons
                    HeaderButtonComponent={AppHeaderIcon}
                    title="Main icon left"
                >
                    <Item
                        title="Main menu"
                        iconName="ios-menu"
                        onPress={() => navigation.toggleDrawer()}
                    />
                </HeaderButtons>
            ),
        });
    }, []);

    useEffect(() => {
        dispatch(loadRecipes());
    }, [dispatch]);

    const openRecipeHandler = (recipe) => {
        navigation.navigate('Recipe', {
            recipeId: recipe.id,
        });
    };

    if (loading) {
        return (
            <View style={styles.center}>
                <ActivityIndicator color={THEME.MAIN_COLOR} size="large" />
            </View>
        );
    }

    return (
        <RecipeList
            data={searching ? filteredRules : allRecipes}
            onOpen={openRecipeHandler}
        />
    );
};

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
