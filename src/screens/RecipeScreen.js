import React, { useEffect, useCallback } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Button,
    ScrollView,
    Alert,
    TouchableWithoutFeedback,
    Keyboard,
    TextInput,
    FlatList,
    TouchableOpacity,
    Share,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { THEME } from '../theme';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { AppHeaderIcon } from '../components/AppHeaderIcon';
import { removeRecipe, toggleBooked } from '../store/actions/recipeAction';
import { PhotoPicker } from '../components/PhotoPicker';
import { IngredientsList } from '../components/IngredientsList';
import { Ionicons } from '@expo/vector-icons';
import {
    CardItem,
    Item as BaseItem,
    Left,
    Right,
    Container,
    Body,
    Card,
} from 'native-base';

export const RecipeScreen = ({ navigation, route }) => {
    const recipeId = route.params?.recipeId;
    const dispatch = useDispatch();

    const recipe = useSelector((state) =>
        state.recipe.allRecipes.find((r) => r.id === recipeId)
    );
    const toggleHandler = useCallback(() => {
        dispatch(toggleBooked(recipe));
    }, [dispatch, recipe]);

    const booked = useSelector((state) =>
        state.recipe.bookedRecipes.some((r) => r.id === recipeId)
    );

    const removeHandler = () => {
        Alert.alert(
            'Удаление рецепта',
            'Вы точно хотите удалить рецепт?',
            [
                {
                    text: 'Отменить',
                    style: 'cancel',
                },
                {
                    text: 'Удалить',
                    style: 'destructive',
                    onPress: () => {
                        navigation.goBack();
                        dispatch(removeRecipe(recipeId));
                    },
                },
            ],
            { cancelable: false }
        );
    };

    const editHandler = () => {
        navigation.navigate('Edit', {
            recipeId: recipe.id,
        });
    };

    const shareRecipe = async () => {
        try {
            let ingredients = 'Ингредиенты\n';
            recipe.ingredients.forEach((item) => {
                ingredients += `- ${item?.name ?? item?.ingredient} ${
                    item?.description
                }\n`;
            });
            const result = await Share.share({
                message: `${recipe.title}\n
			    ${recipe.description}
			    ${ingredients.length > 12 ? ingredients : ''}
			    ${new Date(recipe.date).toLocaleDateString()} ${
                    recipe?.kcal.toString().length > 0
                        ? '( ' + recipe?.kcal.toString() + ' kcal )'
                        : ''
                } ${
                    recipe?.cookTime.toString().length > 0
                        ? '( ' + recipe?.cookTime.toString() + ' )'
                        : ''
                }`.trim(),
            });
        } catch (error) {
            alert(error.message);
        }
    };

    React.useLayoutEffect(() => {
        const iconName = booked ? 'ios-heart' : 'ios-heart-empty';

        navigation.setOptions({
            title: recipe
                ? `Рецепт: ${
                      recipe?.title.length > 10
                          ? recipe?.title.substring(0, 10).trim() + '...'
                          : recipe?.title
                  }`
                : '',
            headerRight: () => (
                <HeaderButtons
                    HeaderButtonComponent={AppHeaderIcon}
                    title="Recipe favourite"
                >
                    <Item
                        title="Share recipe"
                        iconName="md-share"
                        onPress={shareRecipe}
                    />
                    <Item
                        title="Take recipe"
                        iconName={iconName}
                        onPress={toggleHandler}
                    />
                </HeaderButtons>
            ),
        });
    }, [booked, recipe]);

    if (!recipe) {
        return null;
    }

    return (
        <ScrollView style={{backgroundColor: "#ffffff"}}>
            <Image
                source={
                    recipe.imgMain
                        ? { uri: recipe.imgMain }
                        : require('../../assets/img/def_recipe.jpg')
                }
                style={styles.image}
            />
            <View style={styles.textWrap}>
                <Text style={styles.title}>{recipe.title}</Text>
                <BaseItem style={{ flex: 1, paddingBottom: 5 }}>
                    <Left style={styles.recipeDate}>
                        <Ionicons name="md-calendar" size={25} />
                        <Text style={{ marginLeft: 5 }}>
                            {new Date(recipe.date).toLocaleDateString()}
                        </Text>
                    </Left>
                    {recipe?.kcal.toString().length > 0 && (
                        <Body style={styles.recipeKcal}>
                            <Ionicons name="ios-restaurant" size={25} />
                            <Text style={{ marginLeft: 5 }}>
                                {recipe.kcal.toString()} kcal
                            </Text>
                        </Body>
                    )}
                    {recipe?.cookTime.length > 0 && (
                        <Right style={styles.recipeTime}>
                            <Ionicons name="md-time" size={25} />
                            <Text style={{ marginLeft: 5 }}>
                                {recipe.cookTime}
                            </Text>
                        </Right>
                    )}
                </BaseItem>
                <Text style={{ textAlign: 'justify' }} multiline>
                    {recipe.description}
                </Text>
                {recipe.ingredients.length > 0 && (
                    <View>
                        <Text style={styles.subTitle}>Ингредиенты</Text>
                        <FlatList
                            data={recipe.ingredients}
                            keyExtractor={(item) => item.id.toString()}
                            renderItem={({ item }) => (
                                <Card>
                                    <CardItem>
                                        <Body style={styles.ingItem}>
                                            <Text style={{ width: '35%' }}>
                                                {item.name ?? item.ingredient}
                                            </Text>
                                            <Text style={{ width: '55%' }}>
                                                {item.description}
                                            </Text>
                                        </Body>
                                    </CardItem>
                                </Card>
                            )}
                        />
                    </View>
                )}
            </View>
            <View style={styles.btnGroup}>
                <TouchableOpacity
                    onPress={removeHandler}
                    style={{
                        ...styles.button,
                        backgroundColor: THEME.DANGER_COLOR,
                    }}
                >
                    <Text style={styles.buttonText}>Удалить</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={editHandler} style={styles.button}>
                    <Text style={styles.buttonText}>Редактирокать</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: '100%',
        height: 200,
    },
    textWrap: {
        padding: 10,
    },
    title: {
        fontSize: 30,
        fontFamily: 'loraRegular',
    },
    ingItem: {
        alignItems: 'center',
        height: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 2,
    },
    recipeDate: { flexDirection: 'row', alignItems: 'center' },
    recipeTime: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    recipeKcal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    subTitle: {
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'loraRegular',
        marginBottom: 10,
    },
    wrapper: {
        marginBottom: 10,
    },
    btnGroup: {
        flex: 1,
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    button: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: THEME.MAIN_COLOR,
        marginHorizontal: 5,
        height: 35,
        borderRadius: 5,
    },
    buttonText: {
        color: THEME.WHITE,
        textAlign: 'center',
        width: '100%',
    },
});
