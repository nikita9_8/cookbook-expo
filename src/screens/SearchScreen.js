import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    Button,
    FlatList,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import {
    Container,
    Header,
    Content,
    Input,
    Label,
    Item as BaseItem,
    Textarea,
    Card,
    CardItem,
    Body,
} from 'native-base';
import { useDispatch, useSelector } from 'react-redux';

import { THEME } from '../theme';
import { Ionicons } from '@expo/vector-icons';
import { searchRecipes } from '../store/actions/recipeAction';
import { KeyboardShift } from '../components/KeyboardShift';

export const SearchScreen = ({ navigation }) => {
    const [search, setSearch] = useState({
        searchField: '',
        ingredientField: '',
        ingredients: [],
    });
    const dispatch = useDispatch();
    const searchedRules = useSelector((state) => state.recipe.searchRules);

    useEffect(() => {
        setSearch({ ...search, ...searchedRules });
    }, []);

    const addIngredient = () => {
        setSearch({
            ...search,
            ingredientField: '',
            ingredients: [
                { id: new Date().getTime(), name: search.ingredientField },
                ...search.ingredients,
            ],
        });
    };

    const setSearchField = (text) => {
        setSearch({ ...search, searchField: text.trim() });
    };

    const setIngredientFiled = (text) => {
        setSearch({ ...search, ingredientField: text.trim() });
    };

    const removeIngredient = ({ id }) => {
        setSearch({
            ...search,
            ingredients: search.ingredients.filter((item) => item.id !== id),
        });
    };

    const saveFilter = () => {
        dispatch(
            searchRecipes({
                searchField: search.searchField,
                ingredients: search.ingredients,
            })
        );
        navigation.navigate('Home');
    };

    return (
        <KeyboardShift>
            {() => (
                <ScrollView style={{ backgroundColor: '#ffffff' }}>
                    <View style={styles.wrapper}>
                        <Label>Поиск по названию рецепта</Label>
                        <BaseItem style={styles.item}>
                            <Input
                                value={search.searchField}
                                onChangeText={setSearchField}
                                placeholder="Укажите название рецепта"
                            />
                        </BaseItem>
                        <View style={styles.list}>
                            <View>
                                <Label>Поиск по ингредиентам</Label>
                                <BaseItem style={styles.item}>
                                    <Input
                                        value={search.ingredientField}
                                        onChangeText={setIngredientFiled}
                                        placeholder="Укажите ингредиент"
                                    />
                                </BaseItem>
                                <Button
                                    title="Добавить ингредиент"
                                    color={THEME.MAIN_COLOR}
                                    onPress={() => {
                                        addIngredient();
                                    }}
                                    disabled={
                                        !search.ingredientField.trim().length ||
                                        search.ingredientField.trim().length >
                                            17
                                    }
                                />
                            </View>
                            <View>
                                <FlatList
                                    data={search.ingredients}
                                    keyExtractor={(item) => item.id.toString()}
                                    renderItem={({ item }) => (
                                        <Card>
                                            <CardItem>
                                                <Body style={styles.ingItem}>
                                                    <Text style={{}}>
                                                        {item.name}
                                                    </Text>
                                                    <TouchableOpacity
                                                        onPress={() => {
                                                            removeIngredient(
                                                                item
                                                            );
                                                        }}
                                                    >
                                                        <Ionicons
                                                            name="ios-close-circle-outline"
                                                            size={22}
                                                            color={
                                                                THEME.DANGER_COLOR
                                                            }
                                                        />
                                                    </TouchableOpacity>
                                                </Body>
                                            </CardItem>
                                        </Card>
                                    )}
                                />
                            </View>
                        </View>
                        <Button
                            title="Найти"
                            color={THEME.MAIN_COLOR}
                            onPress={saveFilter}
                        />
                    </View>
                </ScrollView>
            )}
        </KeyboardShift>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        padding: 10,
        flex: 1,
    },
    mainWrapper: { paddingHorizontal: 10 },
    title: {
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'loraRegular',
        marginVertical: 10,
    },
    textarea: {
        padding: 10,
        marginBottom: 10,
    },
    input: {
        padding: 5,
        marginBottom: 5,
    },
    ingItem: {
        alignItems: 'center',
        height: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 2,
    },
    info: {
        marginVertical: 25,
    },
    item: {
        marginVertical: 5,
    },
    list: {
        marginBottom: 10,
    },
});
