import * as FileSystem from 'expo-file-system';

import {
    ADD_RECIPE,
    EDIT_RECIPE,
    LOAD_POSTS,
    REMOVE_RECIPE,
    SEARCH,
    TOGGLE_BOOKED,
    TOGGLE_DATE,
} from '../types';
import { DB } from '../../db';

export const loadRecipes = () => {
    return async (dispatch) => {
        const recipes = await DB.getRecipes();
        dispatch({
            type: LOAD_POSTS,
            payload: recipes,
        });
    };
};

export const searchRecipes = (searchData) => async (dispatch) => {
    dispatch({
        type: SEARCH,
        payload: searchData,
    });
};

export const toggleBooked = (recipe) => async (dispatch) => {
    await DB.updateRecipe(recipe);

    dispatch({
        type: TOGGLE_BOOKED,
        payload: recipe.id,
    });
};

export const toggleDateFilter = () => async (dispatch) => {
    dispatch({
        type: TOGGLE_DATE,
        payload: null,
    });
};

export const editRecipe = (recipe) => async (dispatch) => {
    const [updatedRecipe = recipe] = await DB.updateFullRecipe(recipe);

    dispatch({
        type: EDIT_RECIPE,
        payload: updatedRecipe,
    });
};

export const removeRecipe = (id) => async (dispatch) => {
    await DB.removeRecipe(id);

    dispatch({
        type: REMOVE_RECIPE,
        payload: id,
    });
};

export const addRecipe = (recipe) => async (dispatch) => {
    let payload = { ...recipe };

    if (recipe.imgMain) {
        const fileName = recipe.imgMain.split('/').pop();
        const newPath = FileSystem.documentDirectory + fileName;

        try {
            await FileSystem.moveAsync({
                to: newPath,
                from: recipe.imgMain,
            });
            payload = { ...recipe, imgMain: newPath };
        } catch (e) {
            console.log('Error: in action --- ', e);
        }
    }
    payload.id = await DB.createPost(payload);
    const ingredients = await DB.createIngredients(payload);
    payload = { ...payload, ingredients };

    dispatch({
        type: ADD_RECIPE,
        payload,
    });
};
