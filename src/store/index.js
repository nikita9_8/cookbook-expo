import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { recipeReducer } from './reducers/recipe';

const rootReducer = combineReducers({
    recipe: recipeReducer,
});

export default createStore(rootReducer, applyMiddleware(thunk));
