import {
    ADD_RECIPE,
    EDIT_RECIPE,
    LOAD_POSTS,
    REMOVE_RECIPE,
    SEARCH,
    TOGGLE_BOOKED,
    TOGGLE_DATE,
} from '../types';

const initialState = {
    allRecipes: [],
    bookedRecipes: [],
    searchedRecipes: [],
    searchRules: {},
    sortingDate: false,
    loading: true,
    searching: false,
};

export const recipeReducer = (state = initialState, action) => {
    const sortDate = (a, b) =>
        state.sortingDate
	        ? new Date(a.date) - new Date(b.date)
	        : new Date(b.date) - new Date(a.date);

    switch (action.type) {
        case LOAD_POSTS:
            return {
                ...state,
                allRecipes: action.payload,
                bookedRecipes: action.payload.filter((r) => r.booked),
                loading: false,
            };
        case TOGGLE_DATE: {
        	const sortDateToggle = (a, b) =>
		        !state.sortingDate
			        ? new Date(a.date) - new Date(b.date)
			        : new Date(b.date) - new Date(a.date)
            return {
                ...state,
                allRecipes: [...state.allRecipes].sort(sortDateToggle),
                searchedRecipes: [...state.searchedRecipes].sort(sortDateToggle),
                sortingDate: !state.sortingDate,
            };
        }
        case TOGGLE_BOOKED:
            const allRecipes = state.allRecipes.map((recipe) => {
                if (recipe.id === action.payload) {
                    recipe.booked = !recipe.booked;
                }
                return recipe;
            });

            return {
                ...state,
                allRecipes,
                bookedRecipes: allRecipes.filter((recipe) => recipe.booked),
            };
        case REMOVE_RECIPE:
            return {
                ...state,
                allRecipes: state.allRecipes.filter(
                    (r) => r.id !== action.payload
                ),
                bookedRecipes: state.bookedRecipes.filter(
                    (r) => r.id !== action.payload
                ),
                searchedRecipes: state.searchedRecipes.filter(
                    (r) => r.id !== action.payload
                ),
            };
        case EDIT_RECIPE:
        case ADD_RECIPE:
            let newFilteredRecipe = false;

            if (state.searching) {
                const {
                    ingredients = [],
                    searchField = '',
                } = state.searchRules;
                newFilteredRecipe =
                    action.payload.ingredients.some((ingredientItem) =>
                        ingredients.some(
                            ({ name }) => ingredientItem.ingredient === name
                        )
                    ) ||
                    (searchField.length
                        ? action.payload.title.includes(searchField)
                        : false);
            }

            if (action.type === EDIT_RECIPE) {
                return {
                    ...state,
                    allRecipes: [
                        ...state.allRecipes.filter(
                            (r) => r.id !== action.payload.id
                        ),
                        { ...action.payload },
                    ].sort(sortDate),
                    searchedRecipes: newFilteredRecipe
                        ? [
                              ...state.searchedRecipes.filter(
                                  (r) => r.id !== action.payload.id
                              ),
                              { ...action.payload },
                          ].sort(sortDate)
                        : [
                              ...state.searchedRecipes.filter(
                                  (r) => r.id !== action.payload.id
                              ),
                          ].sort(sortDate),
                    bookedRecipes: [
                        ...state.allRecipes.filter(
                            (r) => r.id !== action.payload.id
                        ),
                        { ...action.payload },
                    ]
                        .filter((recipe) => recipe.booked),
                };
            }

            return {
                ...state,
                allRecipes: [{ ...action.payload }, ...state.allRecipes].sort(
                    sortDate
                ),
                searchedRecipes: newFilteredRecipe
                    ? [{ ...action.payload }, ...state.searchedRecipes].sort(
                          sortDate
                      )
                    : [...state.searchedRecipes].sort(sortDate),
            };
        case SEARCH:
            const { ingredients = [], searchField = '' } = action.payload;

            const filteredRecipes = state.allRecipes.filter((item) => {
                return (
                    item.ingredients.some((ingredientItem) =>
                        ingredients.some(
                            ({ name }) => ingredientItem.ingredient === name
                        )
                    ) ||
                    (searchField.length
                        ? item.title.includes(searchField)
                        : false)
                );
            });

            return {
                ...state,
                searchedRecipes: filteredRecipes.sort(sortDate),
                searchRules: action.payload,
                searching: !!ingredients.length || searchField !== '',
            };
        default:
            return state;
    }
    return state;
};
