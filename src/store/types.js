export const LOAD_POSTS = 'LOAD_POSTS';
export const TOGGLE_BOOKED = 'TOGGLE_BOOKED';
export const REMOVE_RECIPE = 'REMOVE_RECIPE';
export const ADD_RECIPE = 'ADD_RECIPE';
export const SEARCH = 'SEARCH';
export const TOGGLE_DATE = 'TOGGLE_DATE';
export const EDIT_RECIPE = 'EDIT_RECIPE';