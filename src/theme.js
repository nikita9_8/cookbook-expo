export const THEME = {
    MAIN_COLOR: '#3c991d',
    DANGER_COLOR: '#d81b60',
    WHITE: '#fff',
    TOMATO: 'tomato',
    GRAY: 'gray',
};
